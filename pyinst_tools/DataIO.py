'''
This file is part of pyinst_tools.

pyinst_tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyinst_tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyinst_tools. If not, see <http://www.gnu.org/licenses/>.
'''

import os, sys

BytesIO = StringIO = None
if sys.version_info >= (3, 0):
    from io import BytesIO
else:
    from cStringIO import StringIO

class DataIO(object):
    """ Wrapper class for handling BytesIO (python3) and StringIO (python2). """
    def __init__(self, data):
        if BytesIO is not None:
            self._data = BytesIO()
        elif StringIO is not None:
            self._data = StringIO() # Constructing with a string creates a read-only StringIO
        self._data.write(data)

    def length(self):
        """ Get the current length of the data. """
        pos = self.tell()
        self.seek(0, os.SEEK_END)
        length = self.tell()
        self.seek(pos, os.SEEK_SET)
        return length

    def __getattribute__(self, name):
        """ Override __getattribute__ to return BytesIO/StringIO methods for
        attributes that aren't found. """
        try: return object.__getattribute__(self, name)
        except: pass
        # Check if BytesIO/StringIO has a method with this name
        attr = getattr(self._data, name)
        if attr and hasattr(attr, '__call__'): # Check if it's a method
            return attr
        # Raise an AttributeError
        return object.__getattribute__(self, name)
