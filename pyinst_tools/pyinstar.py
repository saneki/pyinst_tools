#!/usr/bin/env python

'''
This file is part of pyinst_tools.

pyinst_tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyinst_tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyinst_tools. If not, see <http://www.gnu.org/licenses/>.
'''

import argparse, os.path
from pyinstarchive import PyInstArchive
from pyinst_util import get_output_path
from pyzar import pyzar_extract

def pyinstar_parser():
    """ Get the parser. """
    parser = argparse.ArgumentParser(description='PyInstaller (*.exe) archive tool')
    parser.add_argument('-i', '--inspect', action='store_const', dest='op', const='inspect', help='inspect an existing archive')
    parser.add_argument('-t', '--list', action='store_const', dest='op', const='list', help='list the contents of an existing archive')
    parser.add_argument('-u', '--update', action='store_const', dest='op', const='update', help='update an existing archive')
    parser.add_argument('-x', '--extract', action='store_const', dest='op', const='extract', help='extract an existing archive')
    parser.add_argument('-z', '--pyz', action='store_const', dest='op', const='pyz', help='update the pyz archive')
    parser.add_argument('-f', '--file', metavar='ARCHIVE', help='archive path')
    parser.add_argument('-o', '--out', nargs='?', metavar='OUTPUT', help='output path')
    parser.add_argument('files', metavar='FILE', nargs='*', help='files')
    return parser

def pyinstar_extract_pyz(pyzpath):
    dirpath = pyzpath + '_extracted'
    pyzar_extract(pyzpath, dirpath)

def pyinstar_extract(arpath, dirpath):
    """ Extract all files in the archive, optionally including the files
        within the internal PYZ archive. """
    with PyInstArchive(path=arpath) as pyinst:
        # Create the directory if it doesn't exist
        if not os.path.exists(dirpath):
            os.makedirs(dirpath)
        toc = pyinst.toc()
        for entry in toc:
            outpath = os.path.join(dirpath, entry.name)
            # print('Writing: {}'.format(outpath))
            with open(outpath, 'wb') as outfile:
                pyinst.extract(entry.name, outfile)
            if entry.name.endswith('.pyz'):
                pyinstar_extract_pyz(pyzpath=outpath)

def pyinstar_list(arpath):
    """ List files in the archive. """
    with PyInstArchive(path=arpath) as pyinst:
        toc = pyinst.toc()
        for entry in toc:
            print('{0}: cmprsdDataSize={1}, uncmprsdDataSize={2}, cmprsFlags={3}, typeCmprsData={4}'.format( \
                entry.name, entry.cmprsdDataSize, entry.uncmprsdDataSize, entry.cmprsFlag, entry.typeCmprsData))

def pyinstar_inspect(arpath):
    """ Inspect an archive and print info about it. """
    with PyInstArchive(path=arpath) as archive:
        pyinstver = archive.pyinstaller_version()
        if pyinstver == 21: pyinstver = '21+'

        print('PyInstaller Archive: {}'.format(arpath))
        print(' PyInstaller version: {}'.format(pyinstver))
        print(' Python version: {}'.format(archive.pyver()))
        print(' File count: {}'.format(archive.file_count()))
        print(' Overlay: Position={0}, Size={1}'.format( \
            archive.overlay_pos(), archive.overlay_size()))
        print(' ToC:     Position={0}, Size={1}'.format( \
            archive.table_of_contents_pos(), archive.table_of_contents_size()))

def pyinstar_update(arpath, outpath, datapaths):
    """ Update an existing archive by replacing files. """
    with PyInstArchive(path=arpath) as archive:
        for (datapath, datakey) in datapaths:
            print('Replacing data of "{0}" with contents of file="{1}"'.format(datakey, datapath))
            with open(datapath, 'rb') as f:
                archive.set(datakey, f.read())
        with open(outpath, 'wb') as f:
            f.write(archive.data())

def get_keypath_tuple(arg):
    """ Get (path, key) from 'path:key', or (path, fixed(path)) from 'path'.
        Used when replacing existing file data in an archive where both
        the filepath and key need to be specified. """
    if ':' in arg: return tuple(arg.strip().split(':', 1))
    else:
        # Ex. 'data/archive.pyz' => 'archive.pyz'
        return (arg, os.path.basename(arg))

def main():
    parser = pyinstar_parser()
    args = parser.parse_args()

    if args.op is 'extract':
        """ Extract """
        if args.out is None: args.out = args.file + '_extracted'
        pyinstar_extract(arpath=args.file, dirpath=args.out)
    elif args.op is 'list':
        """ List """
        pyinstar_list(arpath=args.file)
    elif args.op is 'inspect':
        """ Inspect """
        pyinstar_inspect(arpath=args.file)
    elif args.op is 'update':
        """ Update """
        if len(args.files) > 0:
            if args.out is None: args.out = get_output_path(args.file)
            (arpath, outpath) = (args.file, args.out)
            datapaths = [get_keypath_tuple(f) for f in args.files]
            pyinstar_update(arpath=arpath, outpath=outpath, datapaths=datapaths)
    elif args.op is 'pyz':
        """ Update the .pyz archive """
        if len(args.files) > 0:
            if args.out is None: args.out = get_output_path(args.file)
            (arpath, outpath) = (args.file, args.out)
            pyinstar_update(arpath=arpath, outpath=outpath, datapaths=[(args.files[0], 'out00-PYZ.pyz')])

if __name__ == '__main__':
    main()
