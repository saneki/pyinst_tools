'''
This file is part of pyinst_tools.

pyinst_tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyinst_tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyinst_tools. If not, see <http://www.gnu.org/licenses/>.
'''

import os.path, re, struct, sys

def detect_pyver():
    info = sys.version_info
    return info[0] * 10 + info[1]

def get_output_path(inpath, append='updated'):
    (noext, ext) = os.path.splitext(inpath)
    return '{0}-{1}{2}'.format(noext, append, ext)

def parse_pyver(version):
    match = re.search(r'[0-9]\.[0-9]', version)
    if not match:
        raise ValueError('Unable to parse python version')
    s = version.split('.')
    return int(s[0]) * 10 + int(s[1])

def pyc_has_size_field(magic):
    magic = struct.unpack('<HH', magic)[0] # Only need the 2 most significant bytes, little-endian
    """ Check the pyc magic number to determine if the pyc header
        has a size field. """
    # Python 1.5 magic = 20121, everything above that is Python 1.5 through 2.X
    if magic >= 20121: return False
    # Comment in Lib/importlib/_bootstrap_external.py about the pyc magic:
    # Python 3.3a0 3200 (__qualname__ added)
    #              3210 (added size modulo 2**32 to the pyc header)
    return magic >= 3210

def warn(message):
    print('Warning: {}'.format(message))
