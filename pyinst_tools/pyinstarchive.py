'''
This file is part of pyinst_tools.

pyinst_tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyinst_tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyinst_tools. If not, see <http://www.gnu.org/licenses/>.
'''

import imp, marshal, os, struct, sys, types, zlib
from DataIO import DataIO

class CTOCEntry:
    def __init__(self, position, cmprsdDataSize, uncmprsdDataSize, cmprsFlag, typeCmprsData, name):
        self.position = position
        self.cmprsdDataSize = cmprsdDataSize
        self.uncmprsdDataSize = uncmprsdDataSize
        self.cmprsFlag = cmprsFlag
        self.typeCmprsData = typeCmprsData
        self.name = name

    def is_compressed(self):
        """ Whether or not the data in this entry is compressed. """
        return self.cmprsFlag > 0

    def datasize(self):
        """ Get the datasize. """
        if self.is_compressed(): return self.cmprsdDataSize
        else: return self.uncmprsdDataSize

class PyInstArchive:
    PYINST20_COOKIE_SIZE = 24           # For pyinstaller 2.0
    PYINST21_COOKIE_SIZE = 24 + 64      # For pyinstaller 2.1+
    MAGIC = b'MEI\014\013\012\013\016'   # Magic number which indetifies pyinstaller

    def __init__(self, path=None, data=None):
        if data is None and path is not None:
            with open(path, 'rb') as fptr:
                data = fptr.read()
        self._data = DataIO(data)
        self._checkFile()
        self._getCArchiveInfo()
        self._parseToc()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self._data.close()

    def toc(self):
        return self._tocList

    def file_count(self):
        return len(self._tocList)

    def data(self):
        """ Get all file data. """
        return self._data.getvalue()

    def extract(self, key, outfile):
        """ Extract a file by key. """
        (fptr, entry, position) = (self._data, self.get(key), self.get_position(key))
        if entry is None:
            raise ValueError('No existing entry exists with name {}'.format(key))
        fptr.seek(position, os.SEEK_SET)
        data = fptr.read(entry.cmprsdDataSize)
        if len(data) != entry.cmprsdDataSize:
            raise ValueError('Unable to read all data')
        # Decompress data if needed
        if entry.is_compressed():
            data = zlib.decompress(data)
        # Todo?: Check decompressed size?
        outfile.write(data)

    def get(self, name):
        """ Get the first entry with the given name. """
        entries = [x for x in self._tocList if x.name == name]
        if len(entries) > 0: return entries[0]
        else: return None

    def get_position(self, name):
        (entries, position) = (self.toc(), self.overlay_pos())
        for entry in entries:
            if entry.name == name: return position
            else: position += entry.datasize()
        raise ValueError('No existing entry exists with name {}'.format(name))

    def set(self, key, data):
        """ Sets the data of some existing entry. Does not overwrite the
        table-of-contents at all, and requires the new data to be of the same
        length as the old data. """
        (fptr, entry, position) = (self._data, self.get(key), self.get_position(key))
        if entry is None:
            raise ValueError('No existing entry exists with name {}'.format(key))
        # Compress data if needed
        if entry.is_compressed():
            data = zlib.compress(data)
        # Make sure the sizes are equal
        if len(data) != entry.datasize():
            raise ValueError('Data size mismatch: Orig ({0}) != New ({1})'.format(entry.datasize(), len(data)))
        fptr.seek(position, os.SEEK_SET)
        fptr.write(data)

    def length(self):
        return self._data.length()

    def overlay_size(self):
        return self._overlaySize

    def overlay_pos(self):
        return self._overlayPos

    def pyver(self):
        return self._pyver

    def pyinstaller_version(self):
        return self._pyinstVer

    def table_of_contents_pos(self):
        return self._tableOfContentsPos

    def table_of_contents_size(self):
        return self._tableOfContentsSize

    def _checkFile(self):
        (fptr, fileSize) = (self._data, self.length())
        # First check if it is a 2.0 archive
        fptr.seek(fileSize - self.PYINST20_COOKIE_SIZE, os.SEEK_SET)
        magicFromFile = fptr.read(len(self.MAGIC))

        if magicFromFile == self.MAGIC:
            self._pyinstVer = 20 # It's a pyinstaller 2.0 archive
            return True

        # Now check for pyinstaller 2.1+ before bailing out
        fptr.seek(fileSize - self.PYINST21_COOKIE_SIZE, os.SEEK_SET)
        magicFromFile = fptr.read(len(self.MAGIC))

        if magicFromFile == self.MAGIC:
            self._pyinstVer = 21 # It's a pyinstaller 2.1+
            return True

        return False

    def _getCArchiveInfo(self):
        (fptr, fileSize) = (self._data, self.length())
        if self._pyinstVer == 20:
            fptr.seek(fileSize - self.PYINST20_COOKIE_SIZE, os.SEEK_SET)

            # Read CArchive cookie
            (magic, lengthofPackage, toc, tocLen, self._pyver) = \
            struct.unpack('!8siiii', fptr.read(self.PYINST20_COOKIE_SIZE))

        elif self._pyinstVer == 21:
            fptr.seek(fileSize - self.PYINST21_COOKIE_SIZE, os.SEEK_SET)

            # Read CArchive cookie
            (magic, lengthofPackage, toc, tocLen, self._pyver, pylibname) = \
            struct.unpack('!8siiii64s', fptr.read(self.PYINST21_COOKIE_SIZE))

        # Overlay is the data appended at the end of the PE
        self._overlaySize = lengthofPackage
        self._overlayPos = fileSize - self._overlaySize
        self._tableOfContentsPos = self._overlayPos + toc
        self._tableOfContentsSize = tocLen

        return True

    def _parseToc(self):
        """ Read the table of contents. """
        # Go to the table of contents
        fptr = self._data
        fptr.seek(self._tableOfContentsPos, os.SEEK_SET)

        self._tocList = []
        parsedLen = 0

        # Parse table of contents
        while parsedLen < self._tableOfContentsSize:
            (entrySize, ) = struct.unpack('!i', fptr.read(4))
            nameLen = struct.calcsize('!iiiiBc')

            (entryPos, cmprsdDataSize, uncmprsdDataSize, cmprsFlag, typeCmprsData, name) = \
                struct.unpack( \
                '!iiiBc{}s'.format(entrySize - nameLen), \
                fptr.read(entrySize - 4))

            self._tocList.append(CTOCEntry(  \
                self._overlayPos + entryPos, \
                cmprsdDataSize,              \
                uncmprsdDataSize,            \
                cmprsFlag,                   \
                typeCmprsData,               \
                name.decode('utf-8').rstrip('\0') \
            ))

            parsedLen += entrySize
