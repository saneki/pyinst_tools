'''
This file is part of pyinst_tools.

pyinst_tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyinst_tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyinst_tools. If not, see <http://www.gnu.org/licenses/>.
'''

import imp, marshal, os, struct, sys, types, zlib
from DataIO import DataIO
from pyinst_util import pyc_has_size_field, warn

class PyzArchive:
    """ PYZ archive. Does not modify the existing file if constructed by filepath,
        the archive is loaded into memory and modified there. """
    def __init__(self, path=None, data=None):
        if data is None and path is not None:
            with open(path, 'rb') as fptr:
                data = fptr.read()
        self._data = DataIO(data)
        self._parseToc()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self._data.close()

    def data(self):
        """ Get all file data. """
        return self._data.getvalue()

    def has(self, key):
        """ Check if the table-of-contents contains a specific key. """
        return self.toc()[key] is not None

    def set(self, key, data):
        """ Set the data of a specific key. Will skip 8 bytes of the data
            (parts of the .pyc header) and compress it before writing. """
        (fptr, toc) = (self._data, self.toc())
        if key not in toc:
            raise ValueError('Key "{}" does not exist in the TableOfContents'.format(key))
        magic = data[0:4]
        # Check if the magic number matches
        if magic != self._pycMagic:
            warn('Pyc magic number mismatch')
        # Skip first 8 (or 12) bytes of pyc header
        if pyc_has_size_field(magic):
            data = zlib.compress(data[12:])
        else:
            data = zlib.compress(data[8:])
        (ispkg, pos, length) = toc[key]
        print('Original length={0}, New length={1}'.format(length, len(data)))
        if len(data) > length:
            raise ValueError('Cannot write data with a larger size than existing data, would overflow')
        fptr.seek(pos, os.SEEK_SET)
        fptr.write(data)
        toc[key] = (ispkg, pos, len(data))

    def get(self, key):
        """ Get the (uncompressed) data of a specific key. Does not include the pyc header. """
        (fptr, toc) = (self._data, self.toc())
        if key not in toc:
            raise ValueError('Key "{}" does not exist in the TableOfContents'.format(key))
        (ispkg, pos, length) = toc[key]
        fptr.seek(pos, os.SEEK_SET)
        data = fptr.read(length)
        if len(data) != length:
            raise ValueError('Unexpected data size after read')
        return zlib.decompress(data)

    def extract(self, key, outfile):
        """ Extract the data of a specific key to a pyc file. """
        outfile.write(self._pycMagic) # Pyc magic
        outfile.write(b'\0' * 4)      # Timestamp
        if self.has_size_field():
            outfile.write(b'\0' * 4)  # Size parameter added in Python 3.3
        data = self.get(key)
        outfile.write(data)

    def toc(self):
        """ Get the table-of-contents Dictionary object. """
        return self._toc

    def update(self):
        """ Rewrite the updated table-of-contents object to the data. """
        (fptr, tocPosition) = (self._data, self._tocPosition)
        fptr.seek(tocPosition, os.SEEK_SET)
        fptr.truncate()
        fptr.write(marshal.dumps(self.toc()))

    def has_size_field(self):
        """ Whether or not the child pyc files should have a size field. """
        return pyc_has_size_field(self._pycMagic)

    def _parseToc(self):
        """ Read the table of contents. """
        fptr = self._data
        fptr.seek(0, os.SEEK_SET)

        pyzMagic = fptr.read(4)
        if pyzMagic != b'PYZ\0':
            raise ValueError('Cannot parse table of contents: not a pyz archive')

        self._pycMagic = fptr.read(4) # Pyc magic value

        # Pyc magic check
        if imp.get_magic() != self._pycMagic:
            warn('The script is running in a different python version than the one used to create the executable.')

        (tocPosition, ) = struct.unpack('!i', fptr.read(4))
        fptr.seek(tocPosition, os.SEEK_SET)

        self._tocPosition = tocPosition

        # TypeError: marshal.load() arg must be file
        try: self._toc = marshal.load(fptr)
        except TypeError:
            # Only read all if needed (seems necessary for python 2)
            self._toc = marshal.loads(fptr.read())

        if type(self._toc) == list:
            self._toc = dict(self._toc)
