#!/usr/bin/env python

'''
This file is part of pyinst_tools.

pyinst_tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyinst_tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyinst_tools. If not, see <http://www.gnu.org/licenses/>.
'''

import argparse, os.path
from pyzarchive import PyzArchive
from pyinst_util import detect_pyver, get_output_path, parse_pyver

def pyzar_parser():
    """ Get the parser. """
    parser = argparse.ArgumentParser(description='PYZ archive tool')
    parser.add_argument('-t', '--list', action='store_const', dest='op', const='list', help='list the contents of an existing archive')
    parser.add_argument('-u', '--update', action='store_const', dest='op', const='update', help='update an existing archive')
    parser.add_argument('-x', '--extract', action='store_const', dest='op', const='extract', help='extract an existing archive')
    parser.add_argument('-f', '--file', metavar='ARCHIVE', help='archive path')
    parser.add_argument('-o', '--out', nargs='?', metavar='OUTPUT', help='output path')
    parser.add_argument('files', metavar='FILE', nargs='*', help='files')
    return parser

def pyzar_extract(pyzpath, dirpath):
    """ Extract files to a directory. """
    with PyzArchive(path=pyzpath) as pyz:
        # Create the directory if it doesn't exist
        if not os.path.exists(dirpath):
            os.makedirs(dirpath)
        toc = pyz.toc()
        for key in toc.keys():
            # For Python > 3.3 some keys are bytes object, some are str object
            try: key = key.decode('utf-8')
            except: pass
            outpath = os.path.join(dirpath, key + '.pyc')
            with open(outpath, 'wb') as outfile:
                pyz.extract(key, outfile)

def pyzar_list(pyzpath):
    """ List files in the archive. """
    with PyzArchive(path=pyzpath) as pyz:
        toc = pyz.toc()
        for key in toc.keys():
            (ispkg, pos, length) = toc[key]
            print('{0}: ispkg={1}, position={2}, length={3}'.format(key, ispkg, pos, length))

def pyzar_update(pyzpath, outpath, datapaths):
    """ Update an existing archive by replacing files. """
    with PyzArchive(path=pyzpath) as pyz:
        for (datapath, datakey) in datapaths:
            print('Replacing key="{0}" with contents of file="{1}"'.format(datakey, datapath))
            with open(datapath, 'rb') as f:
                pyz.set(datakey, f.read())
        with open(outpath, 'wb') as f:
            f.write(pyz.data())

def get_keypath_tuple(arg):
    """ Get (path, key) from 'path:key', or (path, fixed(path)) from 'path'.
        Used when replacing existing file data in an archive where both
        the filepath and key need to be specified. """
    if ':' in arg: return tuple(arg.strip().split(':', 1))
    else:
        # Ex. 'compiled/pythonFile.pyc' => 'pythonFile'
        key = os.path.splitext(os.path.basename(arg))[0]
        return (arg, key)

def main():
    parser = pyzar_parser()
    args = parser.parse_args()

    if args.op is 'extract':
        """ Extract """
        if args.out is None: args.out = args.file + '_extracted'
        pyzar_extract(pyzpath=args.file, dirpath=args.out)
    elif args.op is 'list':
        """ List """
        pyzar_list(pyzpath=args.file)
    elif args.op is 'update':
        """ Update """
        if len(args.files) > 0:
            if args.out is None: args.out = get_output_path(args.file)
            (pyzpath, outpath) = (args.file, args.out)
            datapaths = [get_keypath_tuple(f) for f in args.files]
            pyzar_update(pyzpath=pyzpath, outpath=outpath, datapaths=datapaths)

if __name__ == '__main__':
    main()
