'''
This file is part of pyinst_tools.

pyinst_tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyinst_tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyinst_tools. If not, see <http://www.gnu.org/licenses/>.
'''

from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md')) as f:
    long_description = f.read()

setup(
    name='pyinst_tools',
    version='1.0.0',
    description='Toolkit for reversing PyInstaller executables and archives',
    long_description=long_description,
    url='https://saneki.me/git/pyinst_tools',
    author='saneki',
    author_email='s@neki.me',
    license='GPL-3.0',
    keywords='pyinstaller reversing toolkit',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'pyzar=pyinst_tools.pyzar:main',
            'pyinstar=pyinst_tools.pyinstar:main',
            'pyinstxtractor=pyinst_tools.pyinstxtractor:main',
        ],
    },
)
