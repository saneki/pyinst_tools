Changelog
=========

v1.0.0
------
- Able to list and update files in .pyz updates via pyzar.py
- Able to inspect, list and update files in PyInstaller executables via pyinstar.py
- Includes pyinstxtractor.py from Extreme Coders
